<?php
/**
 * Trakapo
 *
 * NOTICE OF LICENSE
 *
 * Copyright 2014 Livelink New Media
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @category    Trakapo
 * @package     Trakapo_Basetracking
 * @copyright   Copyright (c) 2014 Livelink New Media (http://www.livelinknewmedia.com)
 * @license     http://www.apache.org/licenses/LICENSE-2.0 Apache License (ASL 2.0)
 * @author      John Sanderson <@9point6>
 */

class Trakapo_Basetracking_Helper_Data extends Mage_Core_Helper_Abstract
{
    /**
     * Checks if the logged in user is an admin
     */
    public function isAdmin()
    {
        if (Mage::app()->getStore()->isAdmin()) {
            return true;
        } elseif (Mage::getDesign()->getArea() == 'adminhtml') {
            return true;
        }
        return false;
    }

    /**
     * Formats a number
     */
    public function formatNum($num)
    {
        return floatval(number_format($num, 2, '.', ''));
    }

    /**
     * Generates a GUID style sting
     */
    public function generateGUID()
    {
        return preg_replace(
            "/(.{8})(.{4})(.{4})(.{4})(.{12}).*/",
            "$1-$2-$3-$4-$5",
            sha1(time() . uniqid() . chr(mt_rand(97, 122)))
        );
    }

    /**
     * Adds an event to pending so that it gets included in the next page
     * generation to be tracked using the Trakapo script.
     */
    public function addToPendingEvents($eventName, $eventData)
    {
        $newEvent = array(
            'name' => json_encode($eventName),
            'data' => json_encode($eventData),
        );
        $newEvent['hash'] = json_encode(
            md5($newEvent['name'] . $newEvent['data'])
        );

        $existingEvents = Mage::getSingleton('core/session')
            ->getTrakapoPendingEvents();
        if ($existingEvents === null) {
            $existingEvents = array();
        }

        // Check that a same-hashed event hasn't already been made (this is for baskets)
        foreach ($existingEvents as $i => $existingEvent) {
            if ($existingEvent['hash'] === $newEvent['hash']) {
                return;
            }
        }

        // Finally put the object on
        array_push($existingEvents, $newEvent);
        Mage::getSingleton('core/session')
            ->setTrakapoPendingEvents($existingEvents);
    }


    /**
     * Get all customers and orders
     */
    public function getHistoricProfiles($start, $count)
    {
        // Shorthand function
        $_this = Mage::helper('trakapo_basetracking');

        // Get customers
        $collection = Mage::getModel('customer/customer')
            ->getCollection()
            ->addAttributeToSelect('*')
            ->setPageSize($count)
            ->setCurPage($start / $count + 1);

        $customers = array();
        $customerIds = array();
        $counter = 0;
        foreach ($collection as $customer) {
            $current = $_this->convertCustomer($customer);
            $current['session'] = array(
                '_id' => $_this->generateGUID(),
                '_updated' => date('c', $customer->getCreatedAtTimestamp()),
                'did' => $_this->generateGUID(),
            );
            $current['sessions'] = array();

            $customers[$customer->getId()] = $current;
            $customerIds[] = $customer->getId();
            $counter++;
        }

        unset($collection);

        // Get orders and add them to the customers
        $collection = Mage::getModel('sales/order')
            ->getCollection()
            ->addAttributeToSelect('*')
            ->addAttributeToFilter('customer_id', array(
                'in' => $customerIds,
            ));

        foreach ($collection as $order) {
            $customers[$order->getCustomerId()]['sessions'][] = array(
                '_id' => $_this->generateGUID(),
                '_updated' => date('c', strtotime($order->getCreatedAtStoreDate())),
                'basket' => $_this->convertCart($order,
                    ($order->status == 'pending' || $order->status == 'complete') ?
                        'complete' : 'failed'
                ),
                'basket_status' => $_this->convertOrderToBasketStatus($order),
            );
        }

        return $customers;
    }

    /**
     * Converts a Magento customer into a Trakapo user
     */
    public function convertCustomer($customer)
    {
        $shippingAddress = $customer->getPrimaryShippingAddress();
        $billingAddress = $customer->getPrimaryBillingAddress();
        $address = $shippingAddress ? $shippingAddress : $billingAddress;

        $out = array(
            'id' => $customer->getEmail(),
            'email' => $customer->getEmail(),
            'name' => $customer->getName(),
            'info' => array(
                'first_name' => $customer->getFirstname(),
                'last_name' => $customer->getLastname(),
                'newsletter_subscriber' => Mage::getModel('newsletter/subscriber')
                    ->loadByCustomer($customer)->isSubscribed(),
                'type' => $customer->getGroupid(),
            ),
        );

        if ($address) {
            $out['info'] += array(
                'telephone_number' => $address->getTelephone(),
                'address_line_1' => $address->getStreet1(),
                'address_line_2' => $address->getStreet2(),
                'city' => $address->getCity(),
                'region' => $address->getRegion(),
                'country' => $address->getCountry(),
                'postcode' => $address->getPostcode(),
            );
        }

        return $out;
    }

    /**
     * Converts a Magento product into a Trakapo product
     */
    public function convertProduct($product, $lineItem = false, $localCurrency = null)
    {
        // Shorthand function
        $_this = Mage::helper('trakapo_basetracking');

        // TODO: Cache these somewhere, maybe
        if (!isset($localCurrency) && $localCurrency !== null) {
            $localCurrency = Mage::app()
                ->getStore()
                ->getCurrentCurrencyCode();
        }
        $baseCurrency = Mage::app()
            ->getStore()
            ->getBaseCurrencyCode();

        $image = '';
        try {
            $image = $product->getImageUrl();
        } catch (Exception $e) {

        }

        $ret = array(
            'sku' => $product->getSku(),
            'name' => $product->getName(),
            'url' => $product->getProductUrl(),
            'image_url' => $image,
            'price' => array(
                'local_currency' => $localCurrency,
                'local_value' => $_this->formatNum(Mage::helper('core')
                    ->currency($product->getFinalPrice(), false, false)
                ),
                'value' => $_this->formatNum($product->getFinalPrice()),
            ),
            'quantity' => 0,
            'info' => array(
                'internal_id' => $product->getId(),
            ),
        );

        // Line items have a bit more data
        // die(json_encode($lineItem !== false));
        if ($lineItem !== false) {
            $ret = array_replace_recursive($ret, array(
                'sku' => $lineItem['sku'],
                'price' => array(
                    'value' => $_this->formatNum($lineItem['base_price']),
                    'unit_total' => $_this->formatNum($lineItem['base_price_incl_tax']),
                    'row_total' => $_this->formatNum($lineItem['base_row_total_incl_tax']),
                    'row_discount' => $_this->formatNum($lineItem['base_discount_amount']),

                    'local_unit_total' => $_this->formatNum($lineItem['price_incl_tax']),
                    'local_row_total' => $_this->formatNum($lineItem['row_total_incl_tax']),
                    'local_row_discount' => $_this->formatNum($lineItem['discount_amount']),
                ),
                'quantity' => $_this->formatNum(isset($lineItem['qty_ordered']) ? $lineItem['qty_ordered'] : $lineItem['qty']),
                'info' => array(
                    'row_id' => $lineItem['item_id'],
                ),
            ));
        }

        return $ret;
    }

    /**
     * Converts a Magento cart (or order) to Trakapo basket
     */
    public function convertCart($cart, $status = 'open')
    {
        // Shorthand functions for convenience
        $_this = Mage::helper('trakapo_basetracking');

        // Differences between cart and order
        if (method_exists($cart, 'getItems')) {
            // We have a cart
            $items = $cart->getItems();
            $quote = $cart->getQuote();
            $localCurrency = Mage::app()
                ->getStore()
                ->getCurrentCurrencyCode();
            $totals = $quote->getTotals();
            $totals['items'] = $totals['subtotal'];
            $totals['total'] = $totals['grand_total'];

            unset($totals['subtotal']);
            unset($totals['grand_total']);

            /**
             * Do totals
             */
            $totalsOut = array();
            foreach ($totals as $key => $value) {
                $value = $_this->formatNum($value->getValue());
                $totalsOut[$key] = array(
                    'local_currency' => $localCurrency,
                    'local_value' => $_this->formatNum($value),
                    // TODO: get proper base value
                    'value' => $_this->formatNum($value) *
                        ($exchangeRate != -1 ? $exchangeRate : 1),
                );
            }

        } elseif (method_exists($cart, 'getItemsCollection')) {
            // We have an order
            $items = $cart->getItemsCollection();
            $quote = Mage::getModel('sales/quote')->load($cart->getQuoteId());
            $localCurrency = $cart->getOrderCurrencyCode();

            $totalsOut = array();
            $orderData = $cart->getData();
            $totalTypes = array(
                'discount' => 'discount_amount',
                'shipping' => 'shipping_amount',
                'items' => 'subtotal',
                'tax' => 'tax_amount',
                'total' => array(
                    'total_due',
                    'total_paid',
                ),
            );
            foreach ($totalTypes as $key => $value) {
                if (is_array($value)) {
                    $count = 0;
                    while (!isset($value[$count])) {
                        $count++;
                    }
                    $value = $value[$count];
                }

                $totalsOut[$key] = array(
                    'local_currency' => $localCurrency,
                    'local_value' => $_this->formatNum($orderData[$value]),
                    'value' => $_this->formatNum($orderData['base_' . $value]),
                );
            }


        } else {
            throw new Exception('Passed object not Cart or Order');
        }

        // Currency cruft
        $baseCurrency = Mage::app()
            ->getStore()
            ->getBaseCurrencyCode();
        $exchangeRate = $localCurrency !== $baseCurrency ? 1 : -1;

        /**
         * Do line items
         */
        // Detect if order or cart
        $items = is_array($items) ? $items : $items->toArray();
        $itemsOut = array();

        // generate Trakapo line items array
        foreach ($items['items'] as $i => $item) {
            $product = Mage::getModel('catalog/product')
                ->load($item['product_id']);

            // try and get the exchange rate
            if ($exchangeRate == 1) {
                $exchangeRate = $_this->formatNum($item['base_price']) /
                    $_this->formatNum(Mage::helper('core')
                        ->currency($item['base_price'], false, false)
                    );
            }

            $itemsOut[] = $_this->convertProduct($product, $item, $localCurrency);
        }

        $quoteId = $quote->getId();
        $quoteId = $quoteId ? $quoteId : $cart->getQuoteId();
        $storeId = $quote->getStoreId();
        $storeId = $storeId ? $storeId : $cart->getStoreId();

        return array(
            'bid' => "{$storeId}-{$quoteId}",
            'line_items' => $itemsOut,
            'prices' => $totalsOut,
            'status' => $status,
        );
    }

    /**
     * Converts a Magento Order to a Trakapo Basket Status
     */
    public function convertOrderToBasketStatus($order)
    {
        $payment = $order->getPayment();

        // Specific to HealthBay payment module
        // TODO: Generalise
        try {
            $additionalData = unserialize($payment->getAdditionalData());
            $channel = $additionalData->component_mode;
        } catch (Exception $ex) {
            $channel = 'Unknown';
        }

        try {
            $paymentInstance = $payment->getMethodInstance();
            $method = $paymentInstance->getTitle();
        } catch (Exception $ex) {
            $method = 'Unknown';
        }

        $status = $order->getStatus();

        // TODO: fill in missing fields
        return array(
            'bid' => "{$order->getStoreId()}-{$order->getQuoteId()}",
            'status' => ($status == 'pending' ||
                $status == 'complete' ||
                $status == 'processing') ? 'completed' : 'failed',
            'info' => array(
                'status' => $status,
            //  'transaction_id' => '',
                'order_id' => $order->getRealOrderId(),
                'payment' => array(
                    'method' => $method,
            //      'provider' => '',
                    'channel' => $channel,
                ),
                'currency_code' => $order->getOrderCurrencyCode(),
                'amount' => $order->getTotalDue(),
            ),
        );
    }
}
