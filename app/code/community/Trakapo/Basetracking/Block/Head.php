<?php
/**
 * Trakapo
 *
 * NOTICE OF LICENSE
 *
 * Copyright 2014 Livelink New Media
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @category    Trakapo
 * @package     Trakapo_Basetracking
 * @copyright   Copyright (c) 2014 Livelink New Media (http://www.livelinknewmedia.com)
 * @license     http://www.apache.org/licenses/LICENSE-2.0 Apache License (ASL 2.0)
 * @author      John Sanderson <@9point6>
 */

class Trakapo_Basetracking_Block_Head extends Mage_Core_Block_Template
{
    public function isActive()
    {
        return Mage::getStoreConfig(
            'trakapo_basetracking/general/status'
        ) ? true : false;
    }

    public function getSiteId()
    {
        return Mage::getStoreConfig(
            'trakapo_basetracking/general/site_id'
        );
    }

    public function isPendingEvents()
    {
        $pending = Mage::getSingleton(
            'core/session'
        )->getTrakapoPendingEvents();
        return (($pending !== null) && (count($pending) !== 0)) ? true : false;
    }

    public function getPendingEvents()
    {
        $session = Mage::getSingleton('core/session');
        $events = $session->getTrakapoPendingEvents();

        // Keep track of events sent to Trakapo to prevent massive redundancy
        $pastEvents = $session->getTrakapoPastEvents();
        if (!is_array($pastEvents)) {
            $pastEvents = array();
        }


        $newPastEvents = array();
        $eventMap = array();
        foreach ($events as $event) {
            $newPastEvents[$event['hash']] = isset($pastEvents[$event['hash']]) ?
                $pastEvents[$event['hash']] + 1 : 1;

            $eventMap[$event['hash']] = $event;

            // If it's been put on the page more than 3 times then discard it
            if ($newPastEvents[$event['hash']] > 3) {
                unset($pastEvents[$event['hash']]);
                break;
            }
        }

        $newEvents = array();
        foreach ($newPastEvents as $k => $v) {
            $newEvents[] = $eventMap[$k];
        }

        if (count($newEvents) !== count($events)) {
            $session->setTrakapoPendingEvents($newEvents);
        }
        $session->setTrakapoPastEvents($newPastEvents);

        return $newEvents;
    }
}
