<?php
/**
 * Trakapo
 *
 * NOTICE OF LICENSE
 *
 * Copyright 2014 Livelink New Media
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @category    Trakapo
 * @package     Trakapo_Basetracking
 * @copyright   Copyright (c) 2014 Livelink New Media (http://www.livelinknewmedia.com)
 * @license     http://www.apache.org/licenses/LICENSE-2.0 Apache License (ASL 2.0)
 * @author      John Sanderson <@9point6>
 */

class Trakapo_Basetracking_Block_Adminhtml_System_Config_Form_Button extends Mage_Adminhtml_Block_System_Config_Form_Field
{
    /*
     * Set template
     */
    public function __construct()
    {
        parent::__construct();

        if (!$this->getTemplate()) {
            $this->setTemplate('trakapo_basetracking/system/config/button.phtml');
        }
    }

    /**
     * Return element html
     *
     * @param  Varien_Data_Form_Element_Abstract $element
     * @return string
     */
    protected function _getElementHtml(Varien_Data_Form_Element_Abstract $element)
    {
        $this->setElement($element);
        return $this->_toHtml();
    }

    /**
     * Return ajax url for button
     *
     * @return string
     */
    public function getAjaxHistoricUrl()
    {
        return Mage::helper('adminhtml')
            ->getUrl('trakapo_basetracking/adminhtml_form/historic');
    }

    /**
     * Return ajax url for task completion
     *
     * @return string
     */
    public function getAjaxHistoricDoneUrl()
    {
        return Mage::helper('adminhtml')
            ->getUrl('trakapo_basetracking/adminhtml_form/historicDone');
    }

    /**
     * Return site ID for submitting events
     *
     * @return string
     */
    public function getSiteId()
    {
        return Mage::getStoreConfig(
            'trakapo_basetracking/general/site_id'
        );
    }

    /**
     * Generate button html
     *
     * @return string
     */
    public function getButtonHtml()
    {
        $buttonData = array(
            'id'       => 'trakapo_basetracking_button',
            'label'    => $this->helper('adminhtml')->__('Do Historic Import'),
            'onclick'  => 'javascript:window._trakapo_historic(); return false;',
        );

        if (Mage::getStoreConfig('trakapo_basetracking/general/historic_progress') == -1) {
            $buttonData['disabled'] = 'disabled';
            $buttonData['class'] = 'disabled';
        }

        $button = Mage::app()
            ->getLayout()
            ->createBlock('adminhtml/widget_button')
            ->setData($buttonData);

        return $button->toHtml();
    }
}
