<?php
/**
 * Trakapo
 *
 * NOTICE OF LICENSE
 *
 * Copyright 2014 Livelink New Media
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @category    Trakapo
 * @package     Trakapo_Basetracking
 * @copyright   Copyright (c) 2014 Livelink New Media (http://www.livelinknewmedia.com)
 * @license     http://www.apache.org/licenses/LICENSE-2.0 Apache License (ASL 2.0)
 * @author      John Sanderson <@9point6>
 */

class Trakapo_Basetracking_Model_Observer extends Mage_Core_Model_Abstract
{
    public function customerLogin(Varien_Event_Observer $observer)
    {
        $helper = Mage::helper('trakapo_basetracking');
        if ($helper->isAdmin()) {
            return;
        }

        $customer = $observer->getCustomer();
        $helper->addToPendingEvents('identify', $helper->convertCustomer($customer));
    }

    public function productViewed(Varien_Event_Observer $observer)
    {
        $helper = Mage::helper('trakapo_basetracking');

        if ($helper->isAdmin()) {
            return;
        }

        $product = $observer->getEvent()->getProduct();
        $eventData = $helper->convertProduct($product);
        $eventData['type'] = 'product';

        if (isset($eventData['sku']) && $eventData['sku'] != '') {
            $helper->addToPendingEvents('track', $eventData);
        }
    }

    public function cartUpdated(Varien_Event_Observer $observer)
    {
        $helper = Mage::helper('trakapo_basetracking');
        if ($helper->isAdmin()) {
            return;
        }

        $helper->addToPendingEvents(
            'basket',
            $helper->convertCart(Mage::helper('checkout/cart')->getCart())
        );
    }

    public function orderCompleted(Varien_Event_Observer $observer)
    {
        $helper = Mage::helper('trakapo_basetracking');
        if ($helper->isAdmin()) {
            return;
        }

        $order = $observer->getEvent()->getOrder();
        $helper->addToPendingEvents(
            'basket_status',
            $helper->convertOrderToBasketStatus($order)
        );
    }

    public function handleTrackedEvents(Varien_Event_Observer $observer)
    {
        $helper = Mage::helper('trakapo_basetracking');
        if ($helper->isAdmin()) {
            return;
        }

        // Should we identify?
        if (Mage::getSingleton('customer/session')->isLoggedIn()) {
            $customer = Mage::getSingleton('customer/session')->getCustomer();
            $helper->addToPendingEvents('identify', $helper->convertCustomer($customer));
        }

        // Handle Tracked events
        $cookie = Mage::getModel('core/cookie')
            ->get('_trakapo_tracked_events');

        $objs = Mage::getSingleton('core/session')
            ->getTrakapoPendingEvents();

        if (isset($cookie) && $cookie) {
            if (isset($objs) && $objs) {
                $out = array();
                $hashes = json_decode($cookie);
                foreach ($objs as $obj) {
                    $noMatch = true;
                    foreach ($hashes as $eventHash => $value) {
                        if (substr($obj['hash'], 1, -1) === $eventHash) {
                            $noMatch = false;
                            break;
                        }
                    }
                    if ($noMatch) {
                        array_push($out, $obj);
                    }
                }

                // This is a hacky workaround for massive pending queues
                if ($count1 > 20) {
                    $out = array_slice($out, -15);
                }

                Mage::getSingleton('core/session')
                    ->setTrakapoPendingEvents($out);
            }

            Mage::getModel('core/cookie')
                ->set('_trakapo_tracked_events', '{}', time() - 1, '/');
        }

    }
}
