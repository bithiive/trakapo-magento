<?php
/**
 * Trakapo
 *
 * NOTICE OF LICENSE
 *
 * Copyright 2014 Livelink New Media
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @category    Trakapo
 * @package     Trakapo_Basetracking
 * @copyright   Copyright (c) 2014 Livelink New Media (http://www.livelinknewmedia.com)
 * @license     http://www.apache.org/licenses/LICENSE-2.0 Apache License (ASL 2.0)
 * @author      John Sanderson <@9point6>
 */

class Trakapo_Basetracking_Adminhtml_FormController extends Mage_Adminhtml_Controller_Action
{
    /**
     * Runs historic import
     *
     * @return void
     */
    public function historicAction()
    {
        $req = $this->getRequest();
        if (empty($req)) {
            $start = 0;
            $count = null;
        } else {
            $start = $req->getParam('start');
            $count = $req->getParam('count');
        }

        $helper = Mage::helper('trakapo_basetracking');
        $total = Mage::getModel('customer/customer')
            ->getCollection()
            ->getSize();
        $profiles = $helper->getHistoricProfiles($start, $count);

        $out = json_encode(array(
            'data' => $profiles,
            'start' => $start,
            'count' => $count,
            'total' => $total,
        ));

        Mage::app()->getResponse()->setBody($out);
    }
    /**
     * confirms historic import completion
     *
     * @return void
     */
    public function historicDoneAction()
    {
        $helper = Mage::helper('trakapo_basetracking');

        // Mark as completed
        Mage::getConfig()->saveConfig(
            'trakapo_basetracking/general/historic_progress',
            -1
        );
        Mage::getConfig()->reinit();
        Mage::app()->reinitStores();

        Mage::app()->getResponse()->setBody('{}');
    }
}
