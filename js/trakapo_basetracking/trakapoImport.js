/**
 * Trakapo
 *
 * NOTICE OF LICENSE
 *
 * Copyright 2014 Livelink New Media
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @category    Trakapo
 * @package     Trakapo_Basetracking
 * @copyright   Copyright (c) 2014 Livelink New Media (http://www.livelinknewmedia.com)
 * @license     http://www.apache.org/licenses/LICENSE-2.0 Apache License (ASL 2.0)
 * @author      John Sanderson <@9point6>
 */
(function(){
	window._trakapo_import = {
		'state': {
			'profiles': [],
			'processing': false,
			'progressTick': null,
			'callback': null,
			'iterationDelay': 250,
			'conditions': []
		},

		'ajaxPost': function (url, params, callback) {
			// TODO: Add jQuery support

			if (Ajax && Ajax.Request) {
				new Ajax.Request(url, {
					'method': 'post',
					'contentType': 'application/json',
					'postBody': JSON.stringify(params),
					'onSuccess': function(transport) {
						callback(null, transport);
					},
					'onFailure': function(transport) {
						callback('error', transport);
					}
				});
			}
			else {
				throw 'No AJAX engine found!';
			}
		},

		'doTrakapoEvent': function (options, callback) {
			var params;

			if (typeof options.callback === 'function') {
				callback = options.callback;
			}
			if (!options.did && options.session) {
				options.did = options.session.did;
			}

			extOpts = {
				'_trakapo_historic_import': true,
				'site_id': _trakapo_vars.siteId
			}

			if (options.session) {
				extOpts['sid'] = options.session._id;
				extOpts['timestamp'] = +new Date( options.session._updated );
			}

			if (options.did) {
				extOpts['did'] = options.did;
			}

			params = Object.extend(options.eventData, extOpts);

			_trakapo_import.ajaxPost(
				'https://t.trakapo.com/track/' + options.eventName,
				params, function (err, transport) {
					if (typeof callback === 'function') {
						setTimeout(function () {
							callback(null, transport.responseText);
						}, _trakapo_import.state.iterationDelay);
					}
				}
			);
		},

		'historic': function (profiles, total, progressTick, callback) {
			for( var id in profiles ) {
				if(profiles.hasOwnProperty(id)) {
					// TODO: check for duplicate ids
					_trakapo_import.state.profiles.push(profiles[id]);
				}
			}

			_trakapo_import.state.total = total;
			_trakapo_import.state.progressTick = progressTick;
			_trakapo_import.state.callback = callback;

			if(_trakapo_import.state.processing == false) {
				_trakapo_import.state.processing = true;
			} else {
				return;
			}

			_trakapo_import.doHistoric(function () {
				_trakapo_import.state.processing = false;
				_trakapo_import.state.callback();
			})
		},

		'doHistoric': function(callback) {
			var doProfiles, doSessions,
				state = _trakapo_import.state,
				count = 0, totalImports = 0;

			// Session Loop
			doSessions = function (imports, impCount, next) {
				impLength = Object.keys( imports ).length;

				if (impLength <= impCount) {
					return next();
				}

				var currentImport = imports[impCount++];

				_trakapo_import.doTrakapoEvent({
					'eventName': 'import',
					'eventData': currentImport
				}, function () {
					// Increment Progress
					var profileTick = 100 / state.total;
					state.progressTick(
						(profileTick * (count - 1)) +
						(impCount * profileTick / impLength),
						{'profiles': count, 'sessions': ++totalImports}
					);

					doSessions(imports, impCount, next);
				});
			}

			// Profile Loop
			doProfiles = function () {
				var profile, sessions, profileSession, condition;

				if (state.profiles.length <= count && state.profiles.length >= state.total) {
					return callback();
				}
				else if (state.profiles.length <= count && state.profiles.length < state.total) {
					return setTimeout(doProfiles, 5000);
				}

				// Move everything into sensible objects
				profile = state.profiles[count++];
				profile._id = profile.email;
				sessions = profile.sessions;
				profileSession = profile.session;
				profile.sessions = null;
				delete profile.sessions;
				profile.session = null;
				delete profile.session;

				// Do conditions
				for(i in state.conditions) {
					condition = state.conditions[i];

					switch(condition.type) {
						case 'regex':
							// .pattern = regex
							// .test_field = field on profile to test
							// .ignore_match = boolean of if to ignore matches or not
							if(condition.pattern.test(profile[condition.test_field]) == condition.ignore_match) {
								return doProfiles();
							}
							break;
					}
				}


				importObjs = [];
				for(i in sessions) {
					if(!sessions.hasOwnProperty(i)) {
						continue;
					}

					session = sessions[i];
					session.basket.status = session.basket_status.status;

					importObjs.push({
						'timestamp': session._updated,
						'user': profile,
						'device': profileSession.did,
						'session': session._id,
						'basket': session.basket,
						'did': profileSession.did,
						'sid': session._id,
						'id': profile._id,
						'key': +new Date() + 'import',
						'end': true
					});
				}

				if(Object.keys(importObjs).length === 0) {
					importObjs.push({
						'timestamp': profileSession._updated,
						'user': profile,
						'device': profileSession.did,
						'session': profileSession._id,
						'did': profileSession.did,
						'sid': profileSession._id,
						'id': profile._id,
						'key': +new Date() + 'import',
						'end': true
					})
				}

				doSessions(
					importObjs, 0,
					function () {
						doProfiles();
					}
				);
			};

			doProfiles();
		}
	};
}).call(this);
